import unittest
from app import datas
from datetime import date




class TestStringMethods(unittest.TestCase):




    def test_validacao(self):
        ano = 2000
        while ano < 2200:
            data = datas(ano).get('sextaSanta')
            dataSep = data.split("/")
            final = date(int(dataSep[2]), int(dataSep[1]), int(dataSep[0]))
            print(final)

            self.assertEqual(
                final.weekday(), 4)
            ano += 1

def runTests():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestStringMethods)
    unittest.TextTestRunner(verbosity=2, failfast=True).run(suite)

if __name__ == '__main__':
    unittest.main()