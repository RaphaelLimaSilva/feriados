from datetime import date
from datetime import timedelta

dias = {0:"Segunda-Feira", 1:"Terça-Feira", 2:"Quarta-Feira", 3:"Quinta_feira", 4:"Sexta-Feira", 5:"Sábado", 6:"Domingo"}

def pascoa(ano):
    a = ano % 19
    b = ano //100
    c = ano % 100
    d = b // 4
    e = b % 4
    f = ( b + 8 ) // 25
    g = (b -f + 1) // 3
    h = (19 * a + b - d - g + 15) % 30
    i = c //4
    k = c % 4
    l = (32 + (2 * e) + (2 * i) - h - k) % 7
    m = ( a + (11 * h) + (22 * l)) // 451
    mes = (h + l - (7*m)+ 114) //31
    dia = 1 + ( h+l - (7*m) + 114) % 31
    data = date(ano,mes,dia)

    return data

def carnaval(pascoa):
    data = pascoa - timedelta(47)
    return data

def sextaSanta(pascoa):
    data = pascoa - timedelta(2)
    return data

def corpusChristi(pascoa):
    data = pascoa + timedelta(60)
    return data

def datas(ano):
    if ano < 1580 :
        return "ano inferior ao calendário Gregoriano, pode acontecer exeções "
    data = pascoa(ano)
    result = {
    "pascoa" : pascoa(ano).strftime('%d/%m/%Y'),
    "carnaval" : carnaval(data).strftime('%d/%m/%Y'),
    "sextaSanta" : sextaSanta(data).strftime('%d/%m/%Y'),
    "corpusChristi" : corpusChristi(data).strftime('%d/%m/%Y')
    }

    return result